import request from '@/utils/request'

/**
 * 绑定接单人微信与易班id
 * @returns {*}
 */
export function JdrWechat(data) {
  return request({
    url: '/wechat/login',
    method: 'post',
    data
  })
}
export function JdrOauth(data) {
  return request({
    url: '/wechat/oauth',
    method: 'post',
    data
  })
}
