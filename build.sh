#!/bin/bash

# 测试写的页面时，执行该脚本，静待 1 分钟左右

GITEE_REPO=https://gitee.com/kele-bingtang/glyxybxxtqdPC.git

# 确保脚本抛出遇到的错误
set -e

# 生成静态文件
npm run build

# 进入生成的文件夹
cd dist

git init
git add -A
git commit -m "项目打包文件"
git push -f $GITEE_REPO master:gh-pages    # 推送到仓库的 gh-pages 分支

# 返回根目录
cd ..

# 删除打包项目
rm -rf dist
